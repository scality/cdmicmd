from distutils.core import setup
import sys
import os

import scalitycdmi.PkgInfo

if float("%d.%d" % sys.version_info[:2]) < 2.6:
    sys.stderr.write("Your Python version %d.%d.%d is not supported.\n" % sys.version_info[:3])
    sys.stderr.write("cdmicmd requires Python 2.6 or newer.\n")
    sys.exit(1)

try:
    import xml.etree.ElementTree as ET
    print "Using xml.etree.ElementTree for XML processing"
except ImportError, e:
    sys.stderr.write(str(e) + "\n")
    try:
        import elementtree.ElementTree as ET
        print "Using elementtree.ElementTree for XML processing"
    except ImportError, e:
        sys.stderr.write(str(e) + "\n")
        sys.stderr.write("Please install ElementTree module from\n")
        sys.stderr.write("http://effbot.org/zone/element-index.htm\n")
        sys.exit(1)

try:
    ## Remove 'MANIFEST' file to force
    ## distutils to recreate it.
    ## Only in "sdist" stage. Otherwise
    ## it makes life difficult to packagers.
    if sys.argv[1] == "sdist":
        os.unlink("MANIFEST")
except:
    pass

data_files = None

## Main distutils info
setup(
    ## Content description
    name = scalitycdmi.PkgInfo.package,
    version = scalitycdmi.PkgInfo.version,
    packages = [ 'scalitycdmi','poster','bunch','yaml'],
    scripts = ['cdmicmd'],
    data_files = data_files,

    ## Packaging details
    author = "Patrick Dos Santos",
    author_email = "p@scality.com",
    url = scalitycdmi.PkgInfo.url,
    license = scalitycdmi.PkgInfo.license,
    description = scalitycdmi.PkgInfo.short_description,
    long_description = """
%s

Authors:
--------
Pat Dos Santos <p@scality.com>
""" % (scalitycdmi.PkgInfo.long_description)
    )

# vim:et:ts=4:sts=4:ai
