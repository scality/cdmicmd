CDMI Cloud Storage Client


#Installation

	python setup.py install

#Commands

	  Make bucket
	      cdmicmd mb cdmi://BUCKET
	  Delete bucket
	      cdmicmd rb cdmi://BUCKET
	  List objects
	      cdmicmd ls cdmi://BUCKET
	  List all the buckets
	      cdmicmd la
	  Put file into bucket
	      cdmicmd put FILE [FILE...] cdmi://BUCKET
	  Get file from bucket
	      cdmicmd get cmdi://BUCKET/OBJECT
	  Delete file from bucket
	      cdmicmd del cdmi://BUCKET/OBJECT
	  Stat  file from bucket
	      cdmicmd info cdmi://BUCKET/OBJECT
	  -r, --recursive  Recursive removal.

#Exemple

	# cdmicmd mb cdmi://pat
	Bucket 'cdmi://pat/' created

	# cdmicmd la
	README
	sys/
	TEST2/
	dewpoint/
	test/
	test-056puk82moweuuzy0agro9rk3/
	test-5w22eq3isx6k4vv1yhy9hat9n/
	test-947ap35gwndaiznmzumw68ax1/
	test-e0fytewa8knoc1sovaltlhwk6/
	test-u8berpmk01yqmw3ur13x6rgh0/
	test-uii7i0ze66lh52o531bi7ao04/
	test2/
	test23/
	test23ZZZ/


	# cdmicmd ls cdmi://test2/
	block1000
	block1
	block4
	block8

	# cdmicmd put /etc/hosts cdmi://test2/hosts
	Uploading the file 'cdmi://test2/hosts' done in 0secs - 0 MO/s
