'''
Created on 23 janv. 2013

@author: christophe
'''

import random, string


class Backend(object):
    '''
    classdocs
    '''


    def __init__(self):
        '''
        Constructor
        '''
        pass

    def should_split(self, config):
        return False
    
    def get_write_policy(self, config):
        return None
    
    def get_read_policy(self, config):
        return None
    
    def check_md5(self, config):
        return False

    def has_container_support(self):
        return False

class BackendWithContainer(Backend):
    
    def __init__(self):
        super(BackendWithContainer, self).__init__()
        
    def has_container_support(self):
        return True
    
    def setup(self, config):
        return None
    
    def choose_bucket_prefix(self, template, max_len=30):
        """
        Choose a prefix for our test buckets, so they're easy to identify.
    
        Use template and feed it more and more random filler, until it's
        as long as possible but still below max_len.
        """
        rand = ''.join(
            random.choice(string.ascii_lowercase + string.digits)
            for unused in range(255)
            )
    
        while rand:
            s = template.format(random=rand)
            if len(s) <= max_len:
                return s
            rand = rand[:-1]
    
        raise RuntimeError(
            'Bucket prefix template is impossible to fulfill: {template!r}'.format(
                template=template,
                ),
            )

    def create_bucket(self, config, pref):
        self.make_bucket(pref)
        return pref
        
