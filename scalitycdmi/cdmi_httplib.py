
import httplib
import time

from backend import BackendWithContainer

from poster.streaminghttp import register_openers

try:
    import json
except ImportError:
    import simplejson as json

CDMI_CONTAINER = 'application/cdmi-container'
CDMI_OBJECT = 'application/cdmi-object'
CDMI_VERSION = '1.0.1'

class CDMIException(Exception):
    """ Exception for CDMI """
    def __init__(self, msg):
        Exception.__init__(self)
        self.msg = msg

    def __str__(self):
        return self.msg

class CDMI(BackendWithContainer):

    def __init__(self, backend_config, container=None):
        super(CDMI, self).__init__()
        self.debuglevel = 0
        try:
            if container:
                self.container = container
            else:
                self.container = backend_config.container
        except:
            self.container = None
        if backend_config.is_secure:
            if hasattr(backend_config, 'port'):
                self._conn = httplib.HTTPSConnection(backend_config.host, backend_config.port)
            else:
                self._conn = httplib.HTTPSConnection(backend_config.host)
        else:
            if hasattr(backend_config, 'port'):
                self._conn = httplib.HTTPConnection(backend_config.host, backend_config.port)
            else:
                self._conn = httplib.HTTPConnection(backend_config.host)

    def setup(self, config):
        return config.backend.cdmi.container
        
    def put(self , filename , input_file , usermd=None , encodeusermd=None , content_type='image/png'):

        register_openers()
        content_type = content_type

        try:
                size = input_file.len
        except:
                import os
                size = os.fstat(input_file.fileno()).st_size

        headers = {
                   'Content-Type': content_type,
                   'Content-Length': size,
                   }

        nb = 5
        ntries = 0
        while ntries <= nb:
                try:
                        self._conn.request("PUT", '/%s/%s' % (self.container, filename), input_file, headers)
                        resp = self._conn.getresponse()
                        if resp.status == 201:
                                resp.read()
                                break
                except  httplib.HTTPException as err :
                        raise err
                else:
                        if resp.status in (500 ,503, 504):
                                input_file.seek(0, 0)
                                if self.debuglevel > 0:
                                        print "PUT:Retrying following the error %s / sleep for %s sec before retrying \n" % (resp.status , (1 + ntries))
                                msg = resp.read()
                                time.sleep(1 + ntries)
                                ntries += 1
                                continue
			elif resp.status == 409:
				raise CDMIException("Error %s " % resp.status)

                        break
        if ntries > nb:
                raise CDMIException("cannot put data, response code is %d / error is %s" % (resp.status, msg))        
	return ( resp , ntries )



    def delete(self , filename , usermd=None , encodeusermd=None):
        nb = 5
        ntries = 0
        while ntries <= nb:
                try:
                        self._conn.request("DELETE", '/%s/%s' % (self.container, filename), headers={'X-CDMI-Specification-Version': CDMI_VERSION})
                        resp = self._conn.getresponse()
                        if resp.status in (204, 404):
                            resp.read()
                            break
                except  httplib.HTTPException as err :
                        raise err
                else:
                        if resp.status in (500, 503, 504):
                                if self.debuglevel > 0:
                                        print "DELETE:Retrying following the error %s / sleep for %s sec before retrying \n" % (resp.status , (1 + ntries))
                                msg = resp.read()
                                time.sleep(1 + ntries)
                                ntries += 1
                                continue
        if ntries > nb:
            raise CDMIException("cannot delete data, response code is %d / error is %s" % (resp.status, msg))
        return ntries

    def make_bucket(self , filename):
        headers = {
                   'Accept': CDMI_CONTAINER,
                   'Content-Type': CDMI_CONTAINER,
                   'X-CDMI-Specification-Version': CDMI_VERSION
                   }

        nb = 5
        ntries = 0
        while ntries <= nb:
                try:
                        self._conn.request("PUT", "/%s/" % filename, "{}", headers)
                        resp = self._conn.getresponse()
                        if resp.status ==  201:
                                resp.read()
                                break
			elif resp.status ==  204:
				raise CDMIException("bucket already exist")
                except  httplib.HTTPException as err :
                        raise err, resp.read()
                else:
                        if resp.status in (500, 503, 504):
                                if self.debuglevel > 0:
                                        print "PUT:Retrying following the error %s / sleep for %s sec before retrying \n" % (resp.status , (1 + ntries))
                                msg = resp.read()
                                time.sleep(1 + ntries)
                                ntries += 1
                                continue
        if ntries > nb:
                raise CDMIException("cannot put data, response code is %d / error is %s" % (resp.status, msg))        
        return ntries

    def info(self , filename , policy=None , nousermd=False):
		
	headers = {}
	headers["Accept"] = "*/*"

        self._conn.request("HEAD", '/%s/%s' % ( self.container, filename), headers=headers)
        resp = self._conn.getresponse()
        if resp.status == 200:
            return dict(resp.getheaders())
        raise CDMIException("cannot stat data, response code is %d / error is %s" % (
                       resp.status, str(resp.read())))

  
    def get(self , filename , fd , policy=None , nousermd=False):
        nb = 5
        ntries = 0
        while ntries <= nb:
                try:
                    self._conn.request("GET", '/%s/%s' % (self.container, filename))
                    resp = self._conn.getresponse()
                    if resp.status == 200:
                        fd.write(resp.read())
                        break
                except  httplib.HTTPException as err :
                        raise err
                else:
                    if resp.status in (500, 503, 504):
                            if self.debuglevel > 0:
                                    print "GET:Retrying following the error %s / sleep for %s sec before retrying \n" % (resp.status , (1 + ntries))
                            unused = resp.read()
                            time.sleep(1 + ntries)
                            ntries += 1
                            continue
		    elif resp.status == 404:
			    raise CDMIException("Object not found error: %s" % resp.status)
                    break
        if ntries > nb:
            raise CDMIException("cannot read data, response code is %d / error is %s" % (
                           resp.status, str(resp.read())))
	return ntries 

    def get_all_buckets(self):

	return self.get_bucket("")


    def get_bucket(self , filename):

        headers = {
                  'Accept': CDMI_CONTAINER,
                  'Content-Type': CDMI_OBJECT,
                  'X-CDMI-Specification-Version': CDMI_VERSION

                  }

        self._conn.request("GET", "/%s/" % filename , headers=headers)
        resp = self._conn.getresponse()
        if resp.status == 200:
            data = resp.read()
            result = json.loads(data)
            if result['objectType'] == 'application/cdmi-container':
                    return result['children']
            else:
                    raise CDMIException('') 
	elif resp.status == 404:
		raise CDMIException('Bucket Not Found')
	else:
		raise CDMIException('Listing impossible')

    def delete_bucket(self , filename):

	headers = {
                  'Accept': CDMI_CONTAINER,
                  'Content-Type': CDMI_OBJECT,
                  'X-CDMI-Specification-Version': CDMI_VERSION

	}

        self._conn.request("DELETE", '/%s/' % filename, headers=headers )
        resp = self._conn.getresponse()
        if resp.status == 404:
		raise CDMIException('Bucket Not Found')
	elif resp.status in ( 200,204):
		return 0
	elif resp.status == 499:
		raise CDMIException('Bucket Not empty')
	else:
		raise CDMIException("cannot delete bucket, response code is %s / error is %s" % (
                       str(resp.status), str(resp.read())))
