import os
import bunch
import yaml
import sys

def CheckConf():
	config_file = None
    	if os.getenv("HOME"):
        	config_file = os.path.join(os.getenv("HOME"), ".cdmicfg")
	
	try:
		open(config_file,"r")	
	
	except IOError:
		print "Configuration file not available."
		print "Consider using --configure parameter to create one."
		sys.exit(1)

        with file(config_file) as f:
                config = read_config(f)

        if 'backend' not in config:
            raise RuntimeError('backend section not found in config')
        for item in ['cdmi']:
            if item not in config.backend:
                raise RuntimeError("Missing option for the backend config item: {item}".format(item=item))
                
        if ( config.backend.backend == 'cdmi' ) and ( 'cdmi' in config.backend ):
            for item in ['host','port','is_secure','user','password']:
                if item not in config.backend.cdmi:
                    raise RuntimeError("Missing option for the cdmi config item: {item}".format(item=item))
        return config

def read_config(fp):
        config = bunch.Bunch() 
        g = yaml.safe_load_all(fp)
        for new in g:
                config.update(bunch.bunchify(new))
        return config
